package com.dth.booking.Utility;

public class Constants {
	public static final String PARAMETER_SEP = "&";
	public static final String PARAMETER_EQUALS = "=";
	public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";

	public static int ORDER_PAYMENT_TYPE_COD = 1;
	public static int ORDER_PAYMENT_TYPE_PAYUMONEY = 2;
	public static int ORDER_PAYMENT_TYPE_PAYPAL = 3;
	public static int ORDER_PAYMENT_TYPE_CCAVENUE = 4;
	public static int ORDER_PAYMENT_TYPE_PAYGATE = 5;
	public static int ORDER_PAYMENT_TYPE_INSTAMOJO = 6;
	public static int ORDER_PAYMENT_TYPE_PAYPAL_EXPRESS = 7;
	public static int ORDER_PAYMENT_TYPE_STRIPE = 8;
	public static int ORDER_PAYMENT_TYPE_BAMBORA = 9;
	public static int ORDER_PAYMENT_TYPE_RAZORPAY = 10;
}