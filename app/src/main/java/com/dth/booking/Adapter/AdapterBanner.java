package com.dth.booking.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dth.booking.Global.Typefaces;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import com.dth.booking.Global.Global;
import com.dth.booking.Global.SendMail;
import com.dth.booking.Global.SharedPreference;
import com.dth.booking.Global.StaticUtility;
import com.dth.booking.R;

/**
 * Created by divya on 4/8/17.
 */

public class AdapterBanner extends RecyclerView.Adapter<AdapterBanner.Viewholder> {

    Context context;
    JSONArray jsonArray;

    public AdapterBanner(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = null;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_benner, viewGroup, false);
        return new AdapterBanner.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(final Viewholder viewholder, int position) {

        if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                viewholder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
            }
        }

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewholder.frameBenner.getLayoutParams();
        if (position == 0) {
            params.setMargins((int) context.getResources().getDimension(R.dimen.benner_marginLeight_Right), 0, 10, 0);
            viewholder.frameBenner.setLayoutParams(params);
        } else if (position == getItemCount() - 1) {
            params.setMargins(10, 0, (int) context.getResources().getDimension(R.dimen.benner_marginLeight_Right), 0);
            viewholder.frameBenner.setLayoutParams(params);
        } else {
            params.setMargins(10, 0, 10, 0);
            viewholder.frameBenner.setLayoutParams(params);
        }
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(position);
            JSONObject jsonObjectImage = jsonObject.getJSONObject("image_url");
            String image = jsonObjectImage.getString("main_image");
            viewholder.txtBennerName.setText(jsonObject.getString("title"));

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                /*images = images.replace("[", "");
                image = image.replace("]", "");*/
                urla = new URL(image.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(viewholder.imgBenner, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                                viewholder.rlImgHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imgBenner;
        TextView txtBennerName;
        FrameLayout frameBenner;
        RelativeLayout rlImgHolder;
        ProgressBar pbImgHolder;

        public Viewholder(View itemView) {
            super(itemView);
            txtBennerName = (TextView) itemView.findViewById(R.id.txtBennerName);
            imgBenner = (ImageView) itemView.findViewById(R.id.imgBenner);
            frameBenner = (FrameLayout) itemView.findViewById(R.id.frameBenner);
            rlImgHolder = (RelativeLayout) itemView.findViewById(R.id.rlImgHolder);
            pbImgHolder = (ProgressBar) itemView.findViewById(R.id.pbImgHolder);
            txtBennerName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtBennerName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        }
    }
}
