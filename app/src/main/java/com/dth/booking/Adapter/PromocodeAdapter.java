package com.dth.booking.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dth.booking.Global.Global;
import com.dth.booking.Global.SharedPreference;
import com.dth.booking.Global.StaticUtility;
import com.dth.booking.Global.Typefaces;
import com.dth.booking.Model.Promocode;
import com.dth.booking.R;

import java.util.ArrayList;


public class PromocodeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    Fragment mFragment;
    ArrayList<Promocode> promocodes = new ArrayList<>();
    PromocodeSelection promocodeSelection;
    boolean isClick = false;

    public PromocodeAdapter(Context mContext, Fragment mFragment, ArrayList<Promocode> promocodes) {
        this.mContext = mContext;
        this.promocodes = promocodes;
        this.mFragment = mFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_promocode_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;

            /*if(position == promocodes.size()-1){
                viewHolder.mViewDivider.setVisibility(View.GONE);
            }else {
                viewHolder.mViewDivider.setVisibility(View.VISIBLE);
            }*/

            final Promocode promocode = promocodes.get(position);
            viewHolder.mTxtPromocodeTitle.setText(promocode.getTitle());
            viewHolder.mTxtPromocodeDes.setText(promocode.getDetails());
            viewHolder.mTxtPromocodeDiscount.setText(promocode.getDiscount());
            viewHolder.mTxtPromocode.setText(promocode.getPromocode());

            viewHolder.mTxtApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promocodeSelection = (PromocodeSelection) mFragment;
                    promocodeSelection.SelectedPromocode(promocode.getPromocode());

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return promocodes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtPromocodeTitle, mTxtPromocode, mTxtPromocodeDiscount,
                mTxtPromocodeDes, mTxtApply;
        private LinearLayout mLlPromocode;
        private View mViewDivider;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtPromocodeTitle = itemView.findViewById(R.id.txtPromocodeTitle);
            mTxtPromocode = itemView.findViewById(R.id.txtPromocode);
            mTxtPromocodeDiscount = itemView.findViewById(R.id.txtPromocodeDiscount);
            mTxtPromocodeDes = itemView.findViewById(R.id.txtPromocodeDes);
            mLlPromocode = itemView.findViewById(R.id.llPromocode);
            mViewDivider = itemView.findViewById(R.id.viewDivider);
            mTxtApply = itemView.findViewById(R.id.txtApply);

            mTxtPromocodeTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtPromocode.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
            mTxtPromocodeDiscount.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
            mTxtPromocodeDes.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtApply.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));

            mTxtPromocodeTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtPromocode.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtPromocodeDiscount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtPromocodeDes.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            mTxtApply.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));

            if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sAPPLY) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sAPPLY).equals("")) {
                    mTxtApply.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sAPPLY));
                } else {
                    mTxtApply.setText(mContext.getString(R.string.apply));
                }
            } else {
                mTxtApply.setText(mContext.getString(R.string.apply));
            }

            GradientDrawable gd = (GradientDrawable) mTxtApply.getBackground().getCurrent();
            gd.setShape(GradientDrawable.RECTANGLE);
            gd.setCornerRadius(10);
            gd.setStroke(1, Color.parseColor(SharedPreference.GetPreference(mContext,
                    Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
    }

    public interface PromocodeSelection {
        void SelectedPromocode(String Promocode);
    }
}
