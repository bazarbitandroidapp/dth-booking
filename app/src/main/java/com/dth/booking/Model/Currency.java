package com.dth.booking.Model;

public class Currency {
    String currency_id;
    String currency;
    String code;
    String symbol;
    String post_or_pre_text;

    public Currency(String currency_id, String currency, String code, String symbol,
                    String post_or_pre_text) {
        this.currency_id = currency_id;
        this.currency = currency;
        this.code = code;
        this.symbol = symbol;
        this.post_or_pre_text = post_or_pre_text;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getPost_or_pre_text() {
        return post_or_pre_text;
    }
}
