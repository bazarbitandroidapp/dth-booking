package com.dth.booking.Model;

/**
 * Created by divya on 25/10/17.
 */

public class SubCategory {
    String SubCategoryName;
    String SubCategorySlug;
    String SubCategoryId;

    public SubCategory(String subCategoryName, String subCategorySlug, String subCategoryId) {
        SubCategoryName = subCategoryName;
        SubCategorySlug = subCategorySlug;
        SubCategoryId = subCategoryId;
    }

    public String getSubCategoryName() {
        return SubCategoryName;
    }

    public String getSubCategorySlug() {
        return SubCategorySlug;
    }

    public String getSubCategoryId() {
        return SubCategoryId;
    }
}
