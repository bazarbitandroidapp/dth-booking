package com.dth.booking.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.dth.booking.Adapter.AdapterOrderHistoryTitle;
import com.dth.booking.Global.Global;
import com.dth.booking.Global.SharedPreference;
import com.dth.booking.Global.Typefaces;
import com.dth.booking.Model.Footer;
import com.dth.booking.Model.OrderHistory;
import com.dth.booking.Model.OrderHistoryItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

import com.dth.booking.Fragment.OrderDetailFragment;
import com.dth.booking.Global.SendMail;
import com.dth.booking.Global.StaticUtility;
import com.dth.booking.Model.Item;

import com.dth.booking.R;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class OrderHistoryActivity extends AppCompatActivity implements
        View.OnClickListener, View.OnTouchListener,
        OrderDetailFragment.OnFragmentInteractionListener, AdapterOrderHistoryTitle.OrderDetail {

    ImageView imageCartBack, imageNavigation, imageOrderHistoryFilter;
    TextView txtCatName;

    RelativeLayout relativeProgress;
    LinearLayout llNoOrderAvailable;
    RecyclerView recyclerviewOrderHistory;
    TextView txtNoOrderAvailable;

    private LinearLayout llFilterBg, llContent;
    private SlidingDrawer slidingDrawer;
    ImageView imageCancel;
    TextView txtApply, TextViewFilter;
    EditText editFromDate;
    EditText editToDate;
    LinearLayout llFilterMain;
    View view1;

    ArrayList<OrderHistoryItems> orderHistoryItemses;
    AdapterOrderHistoryTitle adapterOrderHistoryTitle;
    LinearLayout llToolbar;

    String name, image, order_item_status, order_id, order_datetime, order_product_amount, redeem_wallet_amount;

    private List<Item> postdata;
    private ArrayList<HashMap<String, String>> postList = new ArrayList<HashMap<String, String>>();
    private boolean hasMore;
    int limit = 10, offset = 0;
    private boolean flag = true, isFooter = true;
    FragmentManager fragmentManager;

    Context context = OrderHistoryActivity.this;

    private EventBus eventBus = EventBus.getDefault();
    private int i = 0;
    private AlertDialog internetAlert;

    boolean Flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_order_history);
        ProgressBar progress = findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.
                    GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        eventBus.register(context);

        Initialization();
        TypeFace();
        OnClickListener();
        AppSettings();
        setDynamicString();

        String RedirectActivity = getIntent().getStringExtra("Redirect");
        if (RedirectActivity.equalsIgnoreCase("MainActivity")) {
            imageNavigation.setVisibility(View.VISIBLE);
            imageCartBack.setVisibility(View.GONE);
            imageNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Flag) {
                        Flag = false;
                        MainActivity.manageDrawer(true);
                    } else {
                        Flag = true;
                        MainActivity.manageDrawer(false);
                    }
                }
            });
        } else {
            imageNavigation.setVisibility(View.GONE);
            imageCartBack.setVisibility(View.VISIBLE);
            imageCartBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        onBackPressed();
                        finish();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        chanageEditTextBorder(editFromDate);
        chanageEditTextBorder(editToDate);

        imageOrderHistoryFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llToolbar.setVisibility(View.VISIBLE);
               /* if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                } else */
                if (!slidingDrawer.isOpened()) {
                    llFilterBg.setBackgroundColor(Color.parseColor("#85000000"));
                    slidingDrawer.animateOpen();
                    llFilterBg.setVisibility(View.VISIBLE);
                }
            }
        });

        if (postdata != null) {
            try {
                adapterOrderHistoryTitle.clearData();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            postdata.clear();
            postList.clear();
        }

        editFromDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    editToDate.setEnabled(true);
                } else {
                    editToDate.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editToDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        OrderHistory("", "", true);

        recyclerviewOrderHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (hasMore && !(hasFooter())) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    int itemCount = layoutManager.getItemCount();
                    int count = itemCount - 5;
                    if (lastVisibleItemPosition >= layoutManager.getItemCount() - 5) {
                        flag = true;
                        if (editFromDate.getText().toString().equals("")) {
                            OrderHistory("", "", false);
                        } else if (!editToDate.getText().toString().equals("")) {
                            String FromDate = Global.changeDateFormate(editFromDate.getText().toString(),
                                    "dd-MM-yyyy", "yyyy-MM-dd");
                            String ToDate = Global.changeDateFormate(editToDate.getText().toString(),
                                    "dd-MM-yyyy", "yyyy-MM-dd");
                            OrderHistory(FromDate, ToDate, false);
                        } else {
                            String FromDate = Global.changeDateFormate(editFromDate.getText().toString(),
                                    "dd-MM-yyyy", "yyyy-MM-dd");
                            OrderHistory(FromDate, "", false);
                        }
                    }
                }
            }
        });

    }

    private void setDynamicString() {
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sORDER_HISTORY) != null) {
        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sORDER_HISTORY).equals("")) {
            txtCatName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sORDER_HISTORY));
        }else {
            txtCatName.setText(R.string.orderhistory);
        }
        } else {
            txtCatName.setText(R.string.orderhistory);
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sAPPLY) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sAPPLY).equals("")) {
                txtApply.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sAPPLY));
            } else {
                txtApply.setText(getString(R.string.apply));
            }
        } else {
            txtApply.setText(getString(R.string.apply));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sFROM_DATE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sFROM_DATE).equals("")) {
                editFromDate.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sFROM_DATE));
            } else {
                editFromDate.setHint(getString(R.string.from_date));
            }
        } else {
            editFromDate.setHint(getString(R.string.from_date));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sTO_DATE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sTO_DATE).equals("")) {
                editToDate.setHint(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sTO_DATE));
            } else {
                editToDate.setHint(getString(R.string.to_date));
            }
        } else {
            editToDate.setHint(getString(R.string.to_date));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sFILTER) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sFILTER).equals("")) {
                TextViewFilter.setText(SharedPreference.GetPreference(context,
                        StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sFILTER));
            } else {
                TextViewFilter.setText(getString(R.string.filter));
            }
        } else {
            TextViewFilter.setText(getString(R.string.filter));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sNO_ORDER_AVAILABLE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sNO_ORDER_AVAILABLE).equals("")) {
                txtNoOrderAvailable.setText(SharedPreference.GetPreference(context,
                        StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_ORDER_AVAILABLE));
            } else {
                txtNoOrderAvailable.setText(getString(R.string.no_order_available));
            }
        } else {
            txtNoOrderAvailable.setText(getString(R.string.no_order_available));
        }
    }

    @Override
    public void onBackPressed() {
        SharedPreference.ClearPreference(context, StaticUtility.PREFERENCEOrderHistory);
        super.onBackPressed();
    }

    //region Initialization
    private void Initialization() {
        recyclerviewOrderHistory = findViewById(R.id.recyclerviewOrderHistory);
        relativeProgress = findViewById(R.id.relativeProgress);
        llNoOrderAvailable = findViewById(R.id.llNoOrderAvailable);
        txtNoOrderAvailable = findViewById(R.id.txtNoOrderAvailable);
        llFilterBg = findViewById(R.id.llFilterBg);
        llContent = findViewById(R.id.llContent);
        slidingDrawer = findViewById(R.id.slidingDrawer);
        imageCancel = findViewById(R.id.imgCancel);
        txtApply = findViewById(R.id.txtApply);
        TextViewFilter = findViewById(R.id.TextViewFilter);
        editFromDate = findViewById(R.id.editFromDate);
        editToDate = findViewById(R.id.editToDate);
        llFilterMain = findViewById(R.id.llFilterMain);
        view1 = findViewById(R.id.view1);
        imageOrderHistoryFilter = findViewById(R.id.imageOrderHistoryFilter);
        imageCartBack = findViewById(R.id.imageCartBack);
        imageNavigation = findViewById(R.id.imageNavigation);
        txtCatName = findViewById(R.id.txtCatName);
        llToolbar = findViewById(R.id.llToolbar);
        llToolbar.setVisibility(View.VISIBLE);
        imageCancel.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtNoOrderAvailable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtApply.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        TextViewFilter.setTypeface(Typefaces.TypefaceCalibri_bold(context));
        editFromDate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        editToDate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtCatName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        llContent.setOnClickListener(this);
        imageCancel.setOnClickListener(this);
        imageOrderHistoryFilter.setOnClickListener(this);
        txtApply.setOnClickListener(this);
        editFromDate.setOnTouchListener(this);
        editToDate.setOnTouchListener(this);
    }
    //endregion

    //region AppSettings
    private void AppSettings() {
        txtNoOrderAvailable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editFromDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        editToDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        llFilterMain.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        TextViewFilter.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtApply.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        view1.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editFromDate.setHintTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editToDate.setHintTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editToDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        editFromDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region movetofragment
    public void movetofragment(Bundle bundle, Fragment fragment) {
        try {
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentManager.popBackStack();
            fragment.setArguments(bundle);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.framelayout1, fragment);
            fragmentTransaction.commit();
        } catch (IllegalStateException i) {
            i.printStackTrace();
        }
    }
    //endregion

    private boolean hasFooter() {
        return postdata.get(postdata.size() - 1) instanceof Footer;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llContent:
                llToolbar.setVisibility(View.VISIBLE);
                slidingDrawer.animateClose();
                llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                }
                break;
            case R.id.imgCancel:
                llToolbar.setVisibility(View.VISIBLE);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                    if (!editFromDate.getText().toString().equals("")) {
                        offset = 0;
                        editFromDate.setText("");
                        editToDate.setText("");
                        OrderHistory("", "", true);
                    }
                }
                break;
            case R.id.txtApply:
                llToolbar.setVisibility(View.VISIBLE);
                if (slidingDrawer.isOpened()) {
                    slidingDrawer.animateClose();
                    llFilterBg.setBackgroundColor(Color.TRANSPARENT);
                    llFilterBg.setVisibility(View.GONE);
                }
                offset = 0;

                if (!editFromDate.getText().toString().equals("")) {
                    if (!editToDate.getText().toString().equals("")) {
                        String FromDate = Global.changeDateFormate(editFromDate.getText().toString(),
                                "dd-MM-yyyy", "yyyy-MM-dd");
                        String ToDate = Global.changeDateFormate(editToDate.getText().toString(),
                                "dd-MM-yyyy", "yyyy-MM-dd");
                        OrderHistory(FromDate, ToDate, true);
                    } else {
                        String FromDate = Global.changeDateFormate(editFromDate.getText().toString(),
                                "dd-MM-yyyy", "yyyy-MM-dd");
                        OrderHistory(FromDate, "", true);
                    }
                } else {
                    Toast.makeText(context, "Please select From Date..!", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.editFromDate:
                editToDate.setText("");
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    editFromDate.setEnabled(true);
                    Global.DateDialogFromTo newFragment = new Global.DateDialogFromTo(context, editFromDate,
                            false);
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                    return true;
                }
                break;
            case R.id.editToDate:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    editToDate.setEnabled(true);
                    Global.DateDialogFromTo newFragment = new Global.DateDialogFromTo(context, editToDate,
                            true);
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void RefreshListing(int itemPositon, String ItemStatus) {
        String pos = SharedPreference.GetPreference(this, StaticUtility.PREFERENCEOrderHistory,
                StaticUtility.strOrderHistoryLastClick);
        try {
            OrderHistory orderHistory = (OrderHistory) postdata.get(Integer.parseInt(pos));
            orderHistory.getArrayOrderHistoryItems().get(itemPositon).setOrder_item_status(ItemStatus);
            /*postdata.add(Integer.parseInt(pos), orderHistory);*/
            recyclerviewOrderHistory.setAdapter(adapterOrderHistoryTitle);
            recyclerviewOrderHistory.getAdapter().notifyDataSetChanged();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    //region FOR OrderHistory...
    private void OrderHistory(String fromdate, String todate, final boolean isSetUp) {
        if (!isSetUp) {
            if (isFooter) {
                isFooter = false;
                postdata.add(new Footer());
                recyclerviewOrderHistory.getAdapter().notifyItemInserted(postdata.size() - 1);
            }
        } else {
            relativeProgress.setVisibility(View.VISIBLE);
        }
        String[] key = {"fromdate", "todate", "offset", "limit"};
        String[] val = {fromdate, todate, String.valueOf(offset), String.valueOf(limit)};

        ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.OrderHistory + Global.queryStringUrl(context));
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERTOKEN) != null) {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers1(context));
        } else {
            postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                    .addHeaders(Global.headers2(context));
        }
        final ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.optString("status");
                                String strMessage = response.optString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    JSONObject jsonObject = response.getJSONObject("payload");
                                    JSONArray jsonArrayOrders = jsonObject.getJSONArray("orders");
                                    ArrayList<Item> orderHistorys = new ArrayList<>();
                                    if (jsonArrayOrders.length() > 0) {
                                        for (int i = 0; i < jsonArrayOrders.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayOrders.getJSONObject(i);
                                            order_id = jsonObjectPayload.getString("order_id");
                                            order_datetime = jsonObjectPayload.getString("order_datetime");
                                            order_product_amount = jsonObjectPayload.
                                                    getString("order_product_amount");
                                            String order_payment_type = jsonObjectPayload.
                                                    getString("order_payment_type");
                                            String is_prebooking = jsonObjectPayload.
                                                    getString("is_prebooking");
                                            redeem_wallet_amount = jsonObjectPayload.
                                                    getString("redeem_wallet_amount");
                                            JSONArray jsonArrayOrderItems = jsonObjectPayload.
                                                    getJSONArray("ordereditems");
                                            orderHistoryItemses = new ArrayList<>();
                                            for (int j = 0; j < jsonArrayOrderItems.length(); j++) {
                                                JSONObject jsonObjectOrder = jsonArrayOrderItems.getJSONObject(j);
                                                name = jsonObjectOrder.getString("name");
                                                String price = jsonObjectOrder.getString("price");
                                                JSONObject jsonObjectImage = jsonObjectOrder.
                                                        getJSONObject("main_image");
                                                image = jsonObjectImage.getString("main_image");
                                                order_item_status = jsonObjectOrder.
                                                        getString("order_item_status");
                                                String order_item_payment_status = jsonObjectOrder.
                                                        getString("order_item_payment_status");
                                                String currency_symbol = jsonObjectOrder.
                                                        getString("currency_symbol");
                                                String currency_place = jsonObjectOrder.
                                                        getString("currency_place");
                                                OrderHistoryItems orderHistoryItems = new
                                                        OrderHistoryItems(order_datetime, price, name,
                                                        order_item_status, image, order_item_payment_status,
                                                        currency_symbol, currency_place);
                                                orderHistoryItemses.add(orderHistoryItems);
                                            }

                                            OrderHistory orderHistory = new OrderHistory(order_id,
                                                    order_payment_type, orderHistoryItemses, is_prebooking);
                                            orderHistorys.add(orderHistory);

                                        }
                                        if (!isSetUp) {
                                            offset = offset + limit;
                                            if (orderHistorys.size() > 0 && hasMore) {
                                                hasMore = true;
                                                isFooter = true;
                                                int size = postdata.size();
                                                postdata.remove(size - 1);
                                                postdata.addAll(orderHistorys);
                                                recyclerviewOrderHistory.getAdapter().notifyDataSetChanged();
                                            } else {
                                                hasMore = false;
                                                isFooter = true;
                                                int size = postdata.size();
                                                postdata.remove(size - 1);//removes footer
                                                recyclerviewOrderHistory.getAdapter().notifyDataSetChanged();
                                            }
                                            if (limit > orderHistorys.size()) {
                                                hasMore = false;
                                            }
                                        } else {
                                            postdata = orderHistorys;
                                            offset = offset + limit;
                                            if (postdata.size() > 0) {
                                                hasMore = true;
                                                llNoOrderAvailable.setVisibility(View.GONE);
                                                recyclerviewOrderHistory.setVisibility(View.VISIBLE);
                                                adapterOrderHistoryTitle = new AdapterOrderHistoryTitle(context,
                                                        postdata);
                                                recyclerviewOrderHistory.setLayoutManager(new LinearLayoutManager
                                                        (context, LinearLayoutManager.VERTICAL, false));
                                                recyclerviewOrderHistory.setAdapter(adapterOrderHistoryTitle);
                                            } else {
                                                hasMore = false;
                                                llNoOrderAvailable.setVisibility(View.VISIBLE);
                                                recyclerviewOrderHistory.setVisibility(View.GONE);
                                            }
                                        }
                                    } else {
                                        if (!isSetUp) {
                                            hasMore = false;
                                            isFooter = true;
                                            int size = postdata.size();
                                            postdata.remove(size - 1);//removes footer
                                            recyclerviewOrderHistory.getAdapter().notifyDataSetChanged();
                                        } else {
                                            hasMore = false;
                                            llNoOrderAvailable.setVisibility(View.VISIBLE);
                                            recyclerviewOrderHistory.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            } catch (JSONException | NullPointerException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                relativeProgress.setVisibility(View.GONE);
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[",
                                        "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                                            StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context,
                                                StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).
                                                equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context,
                                                    StaticUtility.MULTILANGUAGEPREFERENCE,
                                                    StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                                    "Getting error in OrderHistoryActivity.java When parsing Error response.\n"
                                            + e.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregions

    @Override
    public void gotoOrderDetailscreen(String Order_Id) {
        Bundle bundle = new Bundle();
        OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
        bundle.putString("Order_Id", Order_Id);
        bundle.putString("redeem_wallet_amount", redeem_wallet_amount);
        movetofragment(bundle, orderDetailFragment);
    }

    //region chanageEditTextBorder
    public void chanageEditTextBorder(EditText editText) {
        editText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.TextColor)));
        editText.setBackgroundResource(R.drawable.ic_border);
        GradientDrawable gd = (GradientDrawable) editText.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#FFFFFF"));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(50);
        gd.setStroke(2, Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ThemePrimaryColor)));
    }
    //endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = (Button) row.findViewById(R.id.btnSettings);
        final Button btnExit = (Button) row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT)
                != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                    StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE,
                        StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE,
                StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context,
                Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS),
                                    0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

}
