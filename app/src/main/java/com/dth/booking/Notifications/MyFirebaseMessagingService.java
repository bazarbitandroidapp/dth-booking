package com.dth.booking.Notifications;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.dth.booking.Activity.MainActivity;
import com.dth.booking.Global.StaticUtility;
import com.dth.booking.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by atul on 21/12/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    Bitmap bitmap;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
              /*  JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);*/
                String imageUri = remoteMessage.getData().get("image");
                //String message = remoteMessage.getNotification().getBody();
                String message = remoteMessage.getData().get("message");
                String title = remoteMessage.getData().get("title");
                //To get a Bitmap image from the URL received
                bitmap = getBitmapfromUrl(imageUri);
                if(bitmap != null) {
                    if(title == null){
                        title = "";
                    }
                    sendNotification(title,message);
                }else {
                    showSmallNotification(title,message);
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        } else {
            String title = remoteMessage.getNotification().getTitle();
            if(title == null){
                title = "";
            }
            /*handleNotification(remoteMessage.getNotification().getBody());*/
            showSmallNotification(title,remoteMessage.getNotification().getBody());
        }
    }

    @SuppressLint("WrongConstant")
    private void sendNotification(String title, String messageBody) {
        Intent pushNotification = new Intent(StaticUtility.PUSH_NOTIFICATION);
        pushNotification.putExtra("message", messageBody);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        Intent intent = new Intent(this, MainActivity.class);
        int notificationID = new Random().nextInt(9999 - 1000) + 1000;
        int requestCode = new Random().nextInt(50) + 1;
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        String channelId = getString(R.string.default_notification_channel_id);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = null;
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(messageBody).toString());
        bigPictureStyle.bigPicture(bitmap);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel(channelId);
            if (mChannel == null) {
                mChannel = new NotificationChannel(channelId, getApplicationContext().getString(R.string.app_name),
                        importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(getApplicationContext(), channelId);
            builder.setContentTitle(title)  // required
                    .setSmallIcon(R.drawable.ic_notification_logo) // required
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_logo))
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentText(messageBody)
                    .setStyle(bigPictureStyle)
                    .setContentIntent(pendingIntent)
                    .setTicker(messageBody)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder = new NotificationCompat.Builder(getApplicationContext(), channelId);
            builder.setStyle(bigPictureStyle)
                    .setSmallIcon(R.drawable.ic_notification_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_logo))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 0, 1000, 0, 1000})
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        }

        Notification notification = builder.build();
        notificationManager.notify(notificationID, notification);

       /* if (image != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .bigPicture(image));*//*Notification with Image*//*
        }*/
    }

    @SuppressLint("WrongConstant")
    private void showSmallNotification(String title, String message) {

        Intent intent = new Intent(this, MainActivity.class);
        int notificationID = new Random().nextInt(9999 - 1000) + 1000;
        int requestCode = new Random().nextInt(50) + 1;
        NotificationCompat.Builder builder;
        String channalid = getString(R.string.default_notification_channel_id);

        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(getString(R.string.default_notification_channel_id));
            if (mChannel == null) {
                mChannel = new NotificationChannel(getString(R.string.default_notification_channel_id),
                        getString(R.string.app_name), importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, channalid);
            builder.setContentTitle(title)  // required
                    .setSmallIcon(R.drawable.ic_notification_logo) // required
                    .setContentText(message)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(message)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_logo))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setChannelId(channalid)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {
            builder = new NotificationCompat.Builder(this, channalid);
            builder.setContentTitle(title)                           // required
                    .setSmallIcon(R.drawable.ic_notification_logo) // required
                    .setContentText(message)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(message)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_logo))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        }
        notifManager.notify(notificationID, builder.build());
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
